//
//  ViewController.swift
//  PastDates
//
//  Created by VKGraphics on 25/04/17.
//  Copyright © 2017 VKGraphics. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let cal = Calendar.current
//        var date = cal.startOfDay(for: Date())
//        var days = [Int]()
//        for i in 1 ... 14 {
//            let day = cal.component(.day, from: date)
//            days.append(day)
//            date = cal.date(byAdding: .day, value: -1, to: date)!
//        }
//        print(days)

        self.pastDate()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func pastDate(){
        var today = Date()
        var dateArray = [String]()
        for i in 1...14 {
            let tomorrow = Calendar.current.date(byAdding: .day, value: -1, to: today)
            let date = DateFormatter()
            date.dateFormat = "dd-MM-yyyy"
            let stringDate : String = date.string(from: today)
            today = tomorrow!
            dateArray.append(stringDate)
            
        }
        print(dateArray[dateArray.count - 1])
        let pastDate = "\(dateArray[dateArray.count - 1])"
        let dt = "12-04-2017"
        if dt <= pastDate {
            print("Success!")
        }
        else{
            print("else")
        }
    }

}

